#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{

	char cad[5];
	int i = 0, j = 0, aux[4], resultado = 0;

	printf("Introduce una cadena formada por dos pares de numeros arabigos y romanos (ARAR con mayusculas):\n");
	scanf("%s", cad);
	
	while(i < strlen(cad)){
		
		if(cad[i] == 'I'){

			aux[i] = 1;

		}else if(cad[i] == 'V'){
			aux[i] = 5;

		}else if(cad[i] == 'X'){

			aux[i] = 10;

		}else if(cad[i] == 'L'){

			aux[i] = 50;

		}else if(cad[i] == 'C'){

			aux[i] = 100;

		}else if(cad[i] == 'D'){

			aux[i] = 500;

		}else if(cad[i] == 'M'){

			aux[i] = 1000;

		}else{

			aux[i] = cad[i]-48;

		}
		i++;

	}

	if(aux[1] >= aux[3]){ 

		resultado = (aux[0]*aux[1])+(aux[2]*aux[3]);

	}else if(aux[1] < aux[3]){

		resultado = -(aux[0]*aux[1])+(aux[2]*aux[3]);
	} 

	printf("Resultado: ");
	while(j < strlen(cad)){
		printf("%c", cad[j]);
		j++;
	}
	printf(" = %d", resultado);

}
